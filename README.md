# Chi Rho Corp Project

GitLab project to house [Chi Rho web](https://chirho-corp.org) updates, currently managed in [JIRA](https://chirho-corp.atlassian.net/jira/servicedesk/projects/UCRBP/queues/custom/43) and/or board members' personal email. 

## About

Currently, we have a number of open issues for the Chi Rho website, many of which are housed in [JIRA](https://chirho-corp.atlassian.net/jira/servicedesk/projects/UCRBP/queues/custom/43). The top five issues have been imported into GitLab and can be found in the Issues sidebar. These all will be on the agenda at the next board meeting in June. These have also been referenced below as external links to JIRA.

## High Priority Issues

The following JIRA issues are considered high priority:
- [ ] [Pause book promotion + sales options](https://chirho-corp.atlassian.net/browse/UCRBP-2)
- [ ] [Board member bios update](https://chirho-corp.atlassian.net/browse/UCRBP-1)

## Medium Priority Issues

The following issues are considered medium priority:
- [ ] [Social media footers](https://chirho-corp.atlassian.net/browse/UCRBP-5)
- [ ] [PayPal/Venmo option?](https://chirho-corp.atlassian.net/browse/UCRBP-4)
- [ ] [Invite guest blog post](https://chirho-corp.atlassian.net/browse/UCRBP-3)

## Additional Issues
- Also added to this list is an emailed request to re-certify our organization with [Benevity](https://causes.benevity.org/). This will allow supporters' employers to list us as a trusted charity should supporters wish to donate that way. 
- A second email notes our subscription to [Squarespace](https://account.squarespace.com/) is set to renew automatically on January 15, 2023.


***


